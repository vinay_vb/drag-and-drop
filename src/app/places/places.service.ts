import { Injectable } from '@angular/core';
import { Place } from './place.model';

@Injectable({
  providedIn: 'root'
})
export class PlacesService {

  private _places: Place[] = [
      new Place('p1', 'Manhattan', ' In NYC', 'https://upload.wikimedia.org/wikipedia/commons/thumb/b/b9/Above_Gotham.jpg/1024px-Above_Gotham.jpg', 149.99),
      new Place('p2', 'Paris', ' In Paris', 'https://www.india.com/wp-content/uploads/2018/08/Paris-1.jpg', 189.99),
      new Place('p3', 'Bengaluru', 'Not your avg city', 'https://media-cdn.tripadvisor.com/media/photo-s/03/9b/2f/3a/bangalore.jpg', 99.99)

  ];

  constructor() { }

  get places() {
    return [...this._places];
  }
}
