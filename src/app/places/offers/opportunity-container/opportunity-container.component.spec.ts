import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OpportunityContainerComponent } from './opportunity-container.component';

describe('OpportunityContainerComponent', () => {
  let component: OpportunityContainerComponent;
  let fixture: ComponentFixture<OpportunityContainerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OpportunityContainerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OpportunityContainerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
