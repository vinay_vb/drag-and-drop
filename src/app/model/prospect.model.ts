export class Prospect {
  public name: string;

  constructor(name: string) {
    this.name = name;
  }
}
