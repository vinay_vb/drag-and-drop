import { Component, OnInit, Input } from '@angular/core';
import { CdkDragDrop, moveItemInArray, transferArrayItem } from '@angular/cdk/drag-drop';
import { HttpClient } from '@angular/common/http';
import { Prospect } from '../../../model/prospect.model';

@Component({
  selector: 'app-opportunity-list',
  templateUrl: './opportunity-list.component.html',
  styleUrls: ['./opportunity-list.component.scss']
})
export class OpportunityListComponent implements OnInit {

  @Input() listName;

  targetList: Prospect[] = new Array();

  constructor(private httpClient: HttpClient) { }

  ngOnInit() {
    console.log(this.listName);

    if (this.listName === 'list-1') {
      // this.fetchDataFromServer('new');
      this.targetList = [{name: 'John'},{name: 'Joe'},{name: 'Jerry'}];
    } else if (this.listName === 'list-2') {
      // this.fetchDataFromServer('quoting');
      this.targetList = [{name: 'Edward'},{name: 'Kelly'},{name: 'Jim'}];
    } else {
      // this.fetchDataFromServer('followUp');
      this.targetList = [{name: 'Ken'},{name: 'Mark'},{name: 'Ellie'}];
    }
  }

  drop(event: CdkDragDrop<string[]>) {
    if (event.previousContainer !== event.container) {
      transferArrayItem(event.previousContainer.data, event.container.data, event.previousIndex, event.currentIndex);
    } else {
      moveItemInArray(event.container.data, event.previousIndex, event.currentIndex);
    }
  }

  fetchDataFromServer(status: string) {
    /*return this.httpClient.get<Prospect[]>('/prospects-service/api/users/5/prospects', {
      params: {
        'status': status
      }
    })
      .subscribe((prospects: Prospect[]) => {
        this.targetList = prospects;
      });*/
  }
}
