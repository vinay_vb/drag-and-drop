import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { HttpClientModule } from '@angular/common/http';

import { IonicModule } from '@ionic/angular';

import { OffersPage } from './offers.page';
import { OpportunityListComponent } from './opportunity-list/opportunity-list.component';
import { OpportunityComponent } from './opportunity/opportunity.component';
import { OpportunityContainerComponent } from './opportunity-container/opportunity-container.component';

const routes: Routes = [
  {
    path: '',
    component: OffersPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DragDropModule,
    HttpClientModule,
    RouterModule.forChild(routes)
  ],
  declarations: [OffersPage, OpportunityListComponent, OpportunityComponent, OpportunityContainerComponent]
})
export class OffersPageModule {}
