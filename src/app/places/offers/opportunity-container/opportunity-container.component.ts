import { Component, OnInit } from '@angular/core';
import { CdkDragDrop, moveItemInArray, transferArrayItem } from '@angular/cdk/drag-drop';

@Component({
  selector: 'app-opportunity-container',
  templateUrl: './opportunity-container.component.html',
  styleUrls: ['./opportunity-container.component.scss']
})
export class OpportunityContainerComponent implements OnInit {

  lists = ['list-1', 'list-2', 'list-3'];

  constructor() { }

  ngOnInit() {
  }

}
